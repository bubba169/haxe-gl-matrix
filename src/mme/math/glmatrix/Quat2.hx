package mme.math.glmatrix;

import mme.math.glmatrix.Quat2Tools;

#if lime
import lime.utils.Float32Array;
abstract Quat2(Float32Array) from Float32Array to Float32Array {
#else
import haxe.ds.Vector;
abstract Quat2(Vector<Float>) from Vector<Float> to Vector<Float> {
#end
    public inline function new() {
        #if lime
        this = new Float32Array(9);
        #else
        this = new Vector<Float>(9);
        this[0] = 0.0;
        this[1] = 0.0;
        this[2] = 0.0;
        this[4] = 0.0;
        this[5] = 0.0;
        this[6] = 0.0;
        this[7] = 0.0;
        #end
        this[3] = 1.0;
    }
    @:arrayAccess
    public inline function getk(key:Int) {
    return this[key];
    }
    @:arrayAccess
    public inline function setk(k:Int, v:Float):Float {
        this[k] = v;
        return v;
    }

    public var x ( get, set ) : Float;
    public var y ( get, set ) : Float;
    public var z ( get, set ) : Float;
    public var w ( get, set ) : Float;
    private inline function get_x() : Float {
        return this[0];
    }
    private inline function set_x( x : Float) : Float {
        return this[0] = x;
    }
    private inline function get_y() : Float {
        return this[1];
    }
    private inline function set_y( y : Float) : Float {
        return this[1] = y;
    }
    private inline function get_z() : Float {
        return this[2];
    }
    private inline function set_z( z : Float) : Float {
        return this[2] = z;
    }
    private inline function get_w() : Float {
        return this[3];
    }
    private inline function set_w( w : Float) : Float {
        return this[3] = w;
    }

    public var dx ( get, set ) : Float;
    public var dy ( get, set ) : Float;
    public var dz ( get, set ) : Float;
    public var dw ( get, set ) : Float;
    private inline function get_dx() : Float {
        return this[4];
    }
    private inline function set_dx( dx : Float) : Float {
        return this[4] = dx;
    }
    private inline function get_dy() : Float {
        return this[5];
    }
    private inline function set_dy( dy : Float) : Float {
        return this[5] = dy;
    }
    private inline function get_dz() : Float {
        return this[6];
    }
    private inline function set_dz( dz : Float) : Float {
        return this[6] = dz;
    }
    private inline function get_dw() : Float {
        return this[7];
    }
    private inline function set_dw( dw : Float) : Float {
        return this[7] = dw;
    }

    @:from
    static public function fromArray(a:Array<Float>) {
        return fromValues( a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7] );
    }
    @:to
    public function toArray() {
        return [this[0], this[1], this[2], this[3], this[4], this[5], this[6], this[7]];
    }
    public function toString() : String {
        return Quat2Tools.str(this);
    }

    /**
    * Creates a new dual quat initialized with the given values
    *
    * @param x1 X component
    * @param y1 Y component
    * @param z1 Z component
    * @param w1 W component
    * @param x2 X component
    * @param y2 Y component
    * @param z2 Z component
    * @param w2 W component
    * @returns new dual quaternion
    * @function
    */
    public static function fromValues( ?out : Quat2, x1 : Float, y1 : Float, z1 : Float, w1 : Float, x2 : Float, y2 : Float, z2 : Float, w2 : Float ) : Quat2 {
        if( out == null ) out = Quat2Tools.create();
        out[0] = x1;
        out[1] = y1;
        out[2] = z1;
        out[3] = w1;
        out[4] = x2;
        out[5] = y2;
        out[6] = z2;
        out[7] = w2;
        return out;
    }

    /**
    * Creates a new dual quat from the given values (quat and translation)
    *
    * @param x1 X component
    * @param y1 Y component
    * @param z1 Z component
    * @param w1 W component
    * @param x2 X component (translation)
    * @param y2 Y component (translation)
    * @param z2 Z component (translation)
    * @returns new dual quaternion
    * @function
    */
    public static function fromRotationTranslationValues( ?out : Quat2, x1 : Float, y1 : Float, z1 : Float, w1 : Float, x2 : Float, y2 : Float, z2 : Float ) : Quat2 {
        if( out == null ) out = Quat2Tools.create();
        out[0] = x1;
        out[1] = y1;
        out[2] = z1;
        out[3] = w1;
        var ax = x2 * 0.5,
            ay = y2 * 0.5,
            az = z2 * 0.5;
        out[4] = ax * w1 + ay * z1 - az * y1;
        out[5] = ay * w1 + az * x1 - ax * z1;
        out[6] = az * w1 + ax * y1 - ay * x1;
        out[7] = -ax * x1 - ay * y1 - az * z1;
        return out;
    }

    /**
    * Creates a dual quat from a quaternion and a translation
    *
    * @param dual quaternion receiving operation result
    * @param q a normalized quaternion
    * @param t tranlation vector
    * @returns dual quaternion receiving operation result
    * @function
    */
    public static function fromRotationTranslation( ?out : Quat2, q : Quat, t : Vec3 ) : Quat2 {
        if( out == null ) out = Quat2Tools.create();
        var ax = t[0] * 0.5,
            ay = t[1] * 0.5,
            az = t[2] * 0.5,
            bx = q[0],
            by = q[1],
            bz = q[2],
            bw = q[3];
        out[0] = bx;
        out[1] = by;
        out[2] = bz;
        out[3] = bw;
        out[4] = ax * bw + ay * bz - az * by;
        out[5] = ay * bw + az * bx - ax * bz;
        out[6] = az * bw + ax * by - ay * bx;
        out[7] = -ax * bx - ay * by - az * bz;
        return out;
    }

    /**
    * Creates a dual quat from a translation
    *
    * @param dual quaternion receiving operation result
    * @param t translation vector
    * @returns dual quaternion receiving operation result
    * @function
    */
    public static function fromTranslation( ?out : Quat2, t : Vec3 ) : Quat2 {
        if( out == null ) out = Quat2Tools.create();
        out[0] = 0;
        out[1] = 0;
        out[2] = 0;
        out[3] = 1;
        out[4] = t[0] * 0.5;
        out[5] = t[1] * 0.5;
        out[6] = t[2] * 0.5;
        out[7] = 0;
        return out;
    }

    /**
    * Creates a dual quat from a quaternion
    *
    * @param dual quaternion receiving operation result
    * @param q the quaternion
    * @returns dual quaternion receiving operation result
    * @function
    */
    public static function fromRotation( ?out : Quat2, q : Quat ) : Quat2 {
        if( out == null ) out = Quat2Tools.create();
        out[0] = q[0];
        out[1] = q[1];
        out[2] = q[2];
        out[3] = q[3];
        out[4] = 0;
        out[5] = 0;
        out[6] = 0;
        out[7] = 0;
        return out;
    }

    /**
    * Creates a new dual quat from a matrix (4x4)
    *
    * @param out the dual quaternion
    * @param a the matrix
    * @returns dual quat receiving operation result
    * @function
    */
    public static function fromMat4( ?out : Quat2, a : Mat4 ) : Quat2 {
        if( out == null ) out = Quat2Tools.create();
        //TODO Optimize this
        var outer = QuatTools.create();
        Mat4Tools.getRotation(a, outer);
        var t = new Vec3();
        Mat4Tools.getTranslation(a, t);
        fromRotationTranslation(out, outer, t);
        return out;
    }
}
