package mme.math.glmatrix;

import mme.math.glmatrix.GLMatrix;

import mme.math.glmatrix.Vec4;
import mme.math.glmatrix.Mat4;
import mme.math.glmatrix.Quat;

/**
 * 4 Dimensional Vector
 * @module Vec4
 */
class Vec4Tools {

/*

//
// GENERATORS in Vec4 class
//

Vec4.fromValues( ?out : Vec4, x : Float, y : Float, z : Float, w : Float ) : Vec4;


//
// GENERATORS
//
create() : Vec4;
vec4.clone( a : Vec4 ) : Vec4;

?vec4.random( ?out : Vec4, scale : Float = 1.0, ) : Vec4;

lerp( t : Float, a : Vec4, b : Vec4, ?out : Vec4 ) : Vec4;


//
// EDIT
//
vec4.set( out : Vec4, x : Float, y : Float, z : Float, w : Float ) : Vec4;
vec4.zero( out : Vec4 ) : Vec4;


//
// OPERATORS
//

// unary
//

vec4.copy( a : Vec4, ?out : Vec4 ) : Vec4;
vec4.ceil( a : Vec4, ?out : Vec4 ) : Vec4;
vec4.floor( a : Vec4, ?out : Vec4 ) : Vec4;
vec4.round( a : Vec4, ?out : Vec4 ) : Vec4;
vec4.negate( a : Vec4, ?out : Vec4 ) : Vec4;
vec4.inverse( a : Vec4, ?out : Vec4 ) : Vec4;
vec4.normalize( a : Vec4, ?out : Vec4 ) : Vec4;

// unary, non-Vec4 result
//
vec4.length( a : Vec4 ) : Float;
vec4.squaredLength( a : Vec4 ) : Float;
vec4.str( a : Vec4 ) : String;


// binary

vec4.add( a : Vec4, b : Vec4, ?out : Vec4 ) : Vec4;
vec4.subtract( a : Vec4, b : Vec4, ?out : Vec4 ) : Vec4;
vec4.multiply( a : Vec4, b : Vec4, ?out : Vec4 ) : Vec4;
vec4.divide( a : Vec4, b : Vec4, ?out : Vec4 ) : Vec4;
vec4.min( a : Vec4, b : Vec4, ?out : Vec4 ) : Vec4;
vec4.max( a : Vec4, b : Vec4, ?out : Vec4 ) : Vec4;


// binary, non-Vec4 arg
//
vec4.scale( a : Vec4, b : Float, ?out : Vec4 ) : Vec4;
vec4.postmultiplyMat4( a : Vec4, m : Mat4, ?out : Vec4 ) : Vec4; // a.k.a. Mat4Tools.multiplyVec4()
vec4.postmultiplyQuat( a : Vec4, q : Quat, ?out : Vec4 ) : Vec4; // a.k.a. QuatTools.multiplyVec4()

// binary, non-Vec4 result
//
vec4.distance( a : Vec4, b : Vec4 ) : Float;
vec4.squaredDistance( a : Vec4, b : Vec4 ) : Float;
vec4.dot( a : Vec4, b : Vec4 ) : Float;
vec4.exactEquals( a : Vec4, b : Vec4 ) : Bool;
vec4.equals( a : Vec4, b : Vec4 ) : Bool;


// ternary
//
vec4.cross( u : Vec4, v : Vec4, w : Vec4, ?out : Vec4 ) : Vec4;
vec4.scaleAndAdd( a : Vec4, b : Vec4, scale : Float, ?out : Vec4 ) : Vec4;

*/

    /**
    * Creates a new, empty Vec4
    *
    * @returns {Vec4} a new 4D vector
    */
    public static inline function create() : Vec4 {
    var out = new Vec4();
    return out;
    }

    /**
    * Creates a new Vec4 initialized with values from an existing vector
    *
    * @param {Vec4} a vector to clone
    * @returns {Vec4} a new 4D vector
    */
    public static function clone( a : Vec4 ) : Vec4 {
    var out = new Vec4();
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
    return out;
    }

    /**
    * Copy the values from one Vec4 to another
    *
    * @param {Vec4} out the receiving vector
    * @param {Vec4} a the source vector
    * @returns {Vec4} out
    */
    public static function copy( a : Vec4, ?out : Vec4 ) : Vec4 {
    if( out == null ) out = create();
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
    return out;
    }

    /**
    * Set the components of a Vec4 to the given values
    *
    * @param {Vec4} out the receiving vector
    * @param {Float} x X component
    * @param {Float} y Y component
    * @param {Float} z Z component
    * @param {Float} w W component
    * @returns {Vec4} out
    */
    public static function set( out : Vec4, x : Float, y : Float, z : Float, w : Float ) : Vec4 {
    out[0] = x;
    out[1] = y;
    out[2] = z;
    out[3] = w;
    return out;
    }

    /**
    * Adds two Vec4's
    *
    * @param {Vec4} out the receiving vector
    * @param {Vec4} a the first operand
    * @param {Vec4} b the second operand
    * @returns {Vec4} out
    */
    public static function add( a : Vec4, b : Vec4, ?out : Vec4 ) : Vec4 {
    if( out == null ) out = create();
    out[0] = a[0] + b[0];
    out[1] = a[1] + b[1];
    out[2] = a[2] + b[2];
    out[3] = a[3] + b[3];
    return out;
    }

    /**
    * Subtracts vector b from vector a
    *
    * @param {Vec4} out the receiving vector
    * @param {Vec4} a the first operand
    * @param {Vec4} b the second operand
    * @returns {Vec4} out
    */
    public static function subtract( a : Vec4, b : Vec4, ?out : Vec4 ) : Vec4 {
    if( out == null ) out = create();
    out[0] = a[0] - b[0];
    out[1] = a[1] - b[1];
    out[2] = a[2] - b[2];
    out[3] = a[3] - b[3];
    return out;
    }

    /**
    * Multiplies two Vec4's
    *
    * @param {Vec4} out the receiving vector
    * @param {Vec4} a the first operand
    * @param {Vec4} b the second operand
    * @returns {Vec4} out
    */
    public static function multiply( a : Vec4, b : Vec4, ?out : Vec4 ) : Vec4 {
    if( out == null ) out = create();
    out[0] = a[0] * b[0];
    out[1] = a[1] * b[1];
    out[2] = a[2] * b[2];
    out[3] = a[3] * b[3];
    return out;
    }

    /**
    * Divides two Vec4's
    *
    * @param {Vec4} out the receiving vector
    * @param {Vec4} a the first operand
    * @param {Vec4} b the second operand
    * @returns {Vec4} out
    */
    public static function divide( a : Vec4, b : Vec4, ?out : Vec4 ) : Vec4 {
    if( out == null ) out = create();
    out[0] = a[0] / b[0];
    out[1] = a[1] / b[1];
    out[2] = a[2] / b[2];
    out[3] = a[3] / b[3];
    return out;
    }

    /**
    * Math.ceil the components of a Vec4
    *
    * @param {Vec4} out the receiving vector
    * @param {Vec4} a vector to ceil
    * @returns {Vec4} out
    */
    public static function ceil( a : Vec4, ?out : Vec4 ) : Vec4 {
    if( out == null ) out = create();
    out[0] = Math.ceil(a[0]);
    out[1] = Math.ceil(a[1]);
    out[2] = Math.ceil(a[2]);
    out[3] = Math.ceil(a[3]);
    return out;
    }

    /**
    * Math.floor the components of a Vec4
    *
    * @param {Vec4} out the receiving vector
    * @param {Vec4} a vector to floor
    * @returns {Vec4} out
    */
    public static function floor( a : Vec4, ?out : Vec4 ) : Vec4 {
    if( out == null ) out = create();
    out[0] = Math.floor(a[0]);
    out[1] = Math.floor(a[1]);
    out[2] = Math.floor(a[2]);
    out[3] = Math.floor(a[3]);
    return out;
    }

    /**
    * Returns the minimum of two Vec4's
    *
    * @param {Vec4} out the receiving vector
    * @param {Vec4} a the first operand
    * @param {Vec4} b the second operand
    * @returns {Vec4} out
    */
    public static function min( a : Vec4, b : Vec4, ?out : Vec4 ) : Vec4 {
    if( out == null ) out = create();
    out[0] = Math.min(a[0], b[0]);
    out[1] = Math.min(a[1], b[1]);
    out[2] = Math.min(a[2], b[2]);
    out[3] = Math.min(a[3], b[3]);
    return out;
    }

    /**
    * Returns the maximum of two Vec4's
    *
    * @param {Vec4} out the receiving vector
    * @param {Vec4} a the first operand
    * @param {Vec4} b the second operand
    * @returns {Vec4} out
    */
    public static function max( a : Vec4, b : Vec4, ?out : Vec4 ) : Vec4 {
    if( out == null ) out = create();
    out[0] = Math.max(a[0], b[0]);
    out[1] = Math.max(a[1], b[1]);
    out[2] = Math.max(a[2], b[2]);
    out[3] = Math.max(a[3], b[3]);
    return out;
    }

    /**
    * Math.round the components of a Vec4
    *
    * @param {Vec4} out the receiving vector
    * @param {Vec4} a vector to round
    * @returns {Vec4} out
    */
    public static function round( a : Vec4, ?out : Vec4 ) : Vec4 {
    if( out == null ) out = create();
    out[0] = Math.round(a[0]);
    out[1] = Math.round(a[1]);
    out[2] = Math.round(a[2]);
    out[3] = Math.round(a[3]);
    return out;
    }

    /**
    * Scales a Vec4 by a scalar number
    *
    * @param {Vec4} out the receiving vector
    * @param {Vec4} a the vector to scale
    * @param {Float} b amount to scale the vector by
    * @returns {Vec4} out
    */
    public static function scale( a : Vec4, b : Float, ?out : Vec4 ) : Vec4 {
    if( out == null ) out = create();
    out[0] = a[0] * b;
    out[1] = a[1] * b;
    out[2] = a[2] * b;
    out[3] = a[3] * b;
    return out;
    }

    /**
    * Adds two Vec4's after scaling the second operand by a scalar value
    *
    * @param {Vec4} out the receiving vector
    * @param {Vec4} a the first operand
    * @param {Vec4} b the second operand
    * @param {Float} scale the amount to scale b by before adding
    * @returns {Vec4} out
    */
    public static function scaleAndAdd( a : Vec4, b : Vec4, scale : Float, ?out : Vec4 ) : Vec4 {
    if( out == null ) out = create();
    out[0] = a[0] + (b[0] * scale);
    out[1] = a[1] + (b[1] * scale);
    out[2] = a[2] + (b[2] * scale);
    out[3] = a[3] + (b[3] * scale);
    return out;
    }

    /**
    * Calculates the euclidian distance between two Vec4's
    *
    * @param {Vec4} a the first operand
    * @param {Vec4} b the second operand
    * @returns {Float} distance between a and b
    */
    public static function distance( a : Vec4, b : Vec4 ) : Float {
    var x = b[0] - a[0];
    var y = b[1] - a[1];
    var z = b[2] - a[2];
    var w = b[3] - a[3];
    return Math.sqrt(x*x + y*y + z*z + w*w);
    }

    /**
    * Calculates the squared euclidian distance between two Vec4's
    *
    * @param {Vec4} a the first operand
    * @param {Vec4} b the second operand
    * @returns {Float} squared distance between a and b
    */
    public static function squaredDistance( a : Vec4, b : Vec4 ) : Float {
    var x = b[0] - a[0];
    var y = b[1] - a[1];
    var z = b[2] - a[2];
    var w = b[3] - a[3];
    return x*x + y*y + z*z + w*w;
    }

    /**
    * Calculates the length of a Vec4
    *
    * @param {Vec4} a vector to calculate length of
    * @returns {Float} length of a
    */
    public static function length( a : Vec4 ) : Float {
    var x = a[0];
    var y = a[1];
    var z = a[2];
    var w = a[3];
    return Math.sqrt(x*x + y*y + z*z + w*w);
    }

    /**
    * Calculates the squared length of a Vec4
    *
    * @param {Vec4} a vector to calculate squared length of
    * @returns {Float} squared length of a
    */
    public static function squaredLength( a : Vec4 ) : Float {
    var x = a[0];
    var y = a[1];
    var z = a[2];
    var w = a[3];
    return x*x + y*y + z*z + w*w;
    }

    /**
    * Negates the components of a Vec4
    *
    * @param {Vec4} out the receiving vector
    * @param {Vec4} a vector to negate
    * @returns {Vec4} out
    */
    public static function negate( a : Vec4, ?out : Vec4 ) : Vec4 {
    if( out == null ) out = create();
    out[0] = -a[0];
    out[1] = -a[1];
    out[2] = -a[2];
    out[3] = -a[3];
    return out;
    }

    /**
    * Returns the inverse of the components of a Vec4
    *
    * @param {Vec4} out the receiving vector
    * @param {Vec4} a vector to invert
    * @returns {Vec4} out
    */
    public static function inverse( a : Vec4, ?out : Vec4 ) : Vec4 {
    if( out == null ) out = create();
    out[0] = 1.0 / a[0];
    out[1] = 1.0 / a[1];
    out[2] = 1.0 / a[2];
    out[3] = 1.0 / a[3];
    return out;
    }

    /**
    * Normalize a Vec4
    *
    * @param {Vec4} out the receiving vector
    * @param {Vec4} a vector to normalize
    * @returns {Vec4} out
    */
    public static function normalize( a : Vec4, ?out : Vec4 ) : Vec4 {
    if( out == null ) out = create();
    var x = a[0];
    var y = a[1];
    var z = a[2];
    var w = a[3];
    var len = x*x + y*y + z*z + w*w;
    if (len > 0) {
        len = 1 / Math.sqrt(len);
    }
    out[0] = x * len;
    out[1] = y * len;
    out[2] = z * len;
    out[3] = w * len;
    return out;
    }

    /**
    * Calculates the dot product of two Vec4's
    *
    * @param {Vec4} a the first operand
    * @param {Vec4} b the second operand
    * @returns {Float} dot product of a and b
    */
    public static function dot( a : Vec4, b : Vec4 ) : Float {
    return a[0] * b[0] + a[1] * b[1] + a[2] * b[2] + a[3] * b[3];
    }

    /**
    * Returns the cross-product of three vectors in a 4-dimensional space
    *
    * @param {Vec4} result the receiving vector
    * @param {Vec4} U the first vector
    * @param {Vec4} V the second vector
    * @param {Vec4} W the third vector
    * @returns {Vec4} result
    */
    public static function cross ( u : Vec4, v : Vec4, w : Vec4, ?out : Vec4 ) : Vec4 {
    if( out == null ) out = create();
        var A = (v[0] * w[1]) - (v[1] * w[0]),
            B = (v[0] * w[2]) - (v[2] * w[0]),
            C = (v[0] * w[3]) - (v[3] * w[0]),
            D = (v[1] * w[2]) - (v[2] * w[1]),
            E = (v[1] * w[3]) - (v[3] * w[1]),
            F = (v[2] * w[3]) - (v[3] * w[2]);
        var G = u[0];
        var H = u[1];
        var I = u[2];
        var J = u[3];

        out[0] = (H * F) - (I * E) + (J * D);
        out[1] = -(G * F) + (I * C) - (J * B);
        out[2] = (G * E) - (H * C) + (J * A);
        out[3] = -(G * D) + (H * B) - (I * A);

        return out;
    };

    /**
    * Performs a linear interpolation between two Vec4's
    *
    * @param {Vec4} out the receiving vector
    * @param {Vec4} a the first operand
    * @param {Vec4} b the second operand
    * @param {Float} t interpolation amount, in the range [0-1], between the two inputs
    * @returns {Vec4} out
    */
    public static function lerp( t : Float, a : Vec4, b : Vec4, ?out : Vec4 ) : Vec4 {
    if( out == null ) out = create();
    var ax = a[0];
    var ay = a[1];
    var az = a[2];
    var aw = a[3];
    out[0] = ax + t * (b[0] - ax);
    out[1] = ay + t * (b[1] - ay);
    out[2] = az + t * (b[2] - az);
    out[3] = aw + t * (b[3] - aw);
    return out;
    }

    /**
    * Generates a random vector with the given scale
    *
    * @param {Vec4} out the receiving vector
    * @param {Float} [scale] Length of the resulting vector. If ommitted, a unit vector will be returned
    * @returns {Vec4} out
    */
    public static function random( ?out : Vec4, scale : Float = 1.0 ) : Vec4 {
    if( out == null ) out = create();
    // Marsaglia, George. Choosing a Point from the Surface of a
    // Sphere. Ann. Math. Statist. 43 (1972), no. 2, 645--646.
    // http://projecteuclid.org/euclid.aoms/1177692644;
    var v1, v2, v3, v4;
    var s1, s2;
    do {
        v1 = GLMatrix.RANDOM() * 2 - 1;
        v2 = GLMatrix.RANDOM() * 2 - 1;
        s1 = v1 * v1 + v2 * v2;
    } while (s1 >= 1);
    do {
        v3 = GLMatrix.RANDOM() * 2 - 1;
        v4 = GLMatrix.RANDOM() * 2 - 1;
        s2 = v3 * v3 + v4 * v4;
    } while (s2 >= 1);

    var d = Math.sqrt((1 - s1) / s2);
    out[0] = scale * v1;
    out[1] = scale * v2;
    out[2] = scale * v3 * d;
    out[3] = scale * v4 * d;
    return out;
    }

    /**
    * Transforms the Vec4 with a Mat4.
    * out = m * a
    *
    * @param {Vec4} out the receiving vector
    * @param {Vec4} a the vector to transform
    * @param {Mat4} m matrix to transform with
    * @returns {Vec4} out
    */
    public static function postmultiplyMat4( a : Vec4, m : Mat4, ?out : Vec4 ) : Vec4 {
    if( out == null ) out = create();
    var x = a[0], y = a[1], z = a[2], w = a[3];
    out[0] = m[0] * x + m[4] * y + m[8] * z + m[12] * w;
    out[1] = m[1] * x + m[5] * y + m[9] * z + m[13] * w;
    out[2] = m[2] * x + m[6] * y + m[10] * z + m[14] * w;
    out[3] = m[3] * x + m[7] * y + m[11] * z + m[15] * w;
    return out;
    }

    /**
    * Transforms the Vec4 with a Quat
    * out = q * a
    *
    * @param {Vec4} out the receiving vector
    * @param {Vec4} a the vector to transform
    * @param {Quat} q quaternion to transform with
    * @returns {Vec4} out
    */
    public static function postmultiplyQuat( a : Vec4, q : Quat, ?out : Vec4 ) : Vec4 {
    if( out == null ) out = create();
    var x = a[0], y = a[1], z = a[2];
    var qx = q[0], qy = q[1], qz = q[2], qw = q[3];

    // calculate quat * vec
    var ix = qw * x + qy * z - qz * y;
    var iy = qw * y + qz * x - qx * z;
    var iz = qw * z + qx * y - qy * x;
    var iw = -qx * x - qy * y - qz * z;

    // calculate result * inverse quat
    out[0] = ix * qw + iw * -qx + iy * -qz - iz * -qy;
    out[1] = iy * qw + iw * -qy + iz * -qx - ix * -qz;
    out[2] = iz * qw + iw * -qz + ix * -qy - iy * -qx;
    out[3] = a[3];
    return out;
    }

    /**
    * Set the components of a Vec4 to zero
    *
    * @param {Vec4} out the receiving vector
    * @returns {Vec4} out
    */
    public static function zero( out : Vec4 ) : Vec4 {
    out[0] = 0.0;
    out[1] = 0.0;
    out[2] = 0.0;
    out[3] = 0.0;
    return out;
    }

    /**
    * Returns a string representation of a vector
    *
    * @param {Vec4} a vector to represent as a string
    * @returns {String} string representation of the vector
    */
    public static function str( a : Vec4 ) : String {
    return 'Vec4(' + a[0] + ', ' + a[1] + ', ' + a[2] + ', ' + a[3] + ')';
    }

    /**
    * Returns whether or not the vectors have exactly the same elements in the same position (when compared with ==)
    *
    * @param {Vec4} a The first vector.
    * @param {Vec4} b The second vector.
    * @returns {Boolean} True if the vectors are equal, false otherwise.
    */
    public static function exactEquals( a : Vec4, b : Vec4 ) : Bool {
    return a[0] == b[0] && a[1] == b[1] && a[2] == b[2] && a[3] == b[3];
    }

    /**
    * Returns whether or not the vectors have approximately the same elements in the same position.
    *
    * @param {Vec4} a The first vector.
    * @param {Vec4} b The second vector.
    * @returns {Boolean} True if the vectors are equal, false otherwise.
    */
    public static function equals( a : Vec4, b : Vec4 ) : Bool {
    var a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3];
    var b0 = b[0], b1 = b[1], b2 = b[2], b3 = b[3];
    return (Math.abs(a0 - b0) <= GLMatrix.EPSILON*Math.max(1.0, Math.max(Math.abs(a0), Math.abs(b0))) &&
            Math.abs(a1 - b1) <= GLMatrix.EPSILON*Math.max(1.0, Math.max(Math.abs(a1), Math.abs(b1))) &&
            Math.abs(a2 - b2) <= GLMatrix.EPSILON*Math.max(1.0, Math.max(Math.abs(a2), Math.abs(b2))) &&
            Math.abs(a3 - b3) <= GLMatrix.EPSILON*Math.max(1.0, Math.max(Math.abs(a3), Math.abs(b3))));
    }

    /**
    * Alias for {@link Vec4.subtract}
    * @function
    */
    public static var sub = subtract;

    /**
    * Alias for {@link Vec4.multiply}
    * @function
    */
    public static var mul = multiply;

    /**
    * Alias for {@link Vec4.divide}
    * @function
    */
    public static var div = divide;

    /**
    * Alias for {@link Vec4.distance}
    * @function
    */
    public static var dist = distance;

    /**
    * Alias for {@link Vec4.squaredDistance}
    * @function
    */
    public static var sqrDist = squaredDistance;

    /**
    * Alias for {@link Vec4.length}
    * @function
    */
    public static var len = length;

    /**
    * Alias for {@link Vec4.squaredLength}
    * @function
    */
    public static var sqrLen = squaredLength;
}
