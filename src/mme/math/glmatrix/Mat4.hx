package mme.math.glmatrix;

import mme.math.glmatrix.Mat4Tools;

#if lime
import lime.utils.Float32Array;
abstract Mat4(Float32Array) from Float32Array to Float32Array {
#else
import haxe.ds.Vector;
abstract Mat4(Vector<Float>) from Vector<Float> to Vector<Float> {
#end
    public inline function new() {
        #if lime
        this = new Float32Array(16);
        #else
        this = new Vector<Float>(16);
        this[1] = 0.0;
        this[2] = 0.0;
        this[3] = 0.0;
        this[4] = 0.0;
        this[6] = 0.0;
        this[7] = 0.0;
        this[8] = 0.0;
        this[9] = 0.0;
        this[11] = 0.0;
        this[12] = 0.0;
        this[13] = 0.0;
        this[14] = 0.0;
        #end
        this[0] = 1.0;
        this[5] = 1.0;
        this[10] = 1.0;
        this[15] = 1.0;
    }
    @:arrayAccess
    public inline function getk(key:Int) {
    return this[key];
    }
    @:arrayAccess
    public inline function setk(k:Int, v:Float):Float {
        this[k] = v;
        return v;
    }
    @:from
    static public function fromArray(a:Array<Float>) {
        return fromValues( a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12], a[13], a[14], a[15] );
    }
    @:to
    public function toArray() {
        return [this[0], this[1], this[2], this[3], this[4], this[5], this[6], this[7], this[8], this[9], this[10], this[11], this[12], this[13], this[14], this[15]];
    }
    public function toString() : String {
        return Mat4Tools.str(this);
    }

    /**
    * Create a new Mat4 with the given values
    *
    * @param m00 Component in column 0, row 0 position (index 0)
    * @param m01 Component in column 0, row 1 position (index 1)
    * @param m02 Component in column 0, row 2 position (index 2)
    * @param m03 Component in column 0, row 3 position (index 3)
    * @param m10 Component in column 1, row 0 position (index 4)
    * @param m11 Component in column 1, row 1 position (index 5)
    * @param m12 Component in column 1, row 2 position (index 6)
    * @param m13 Component in column 1, row 3 position (index 7)
    * @param m20 Component in column 2, row 0 position (index 8)
    * @param m21 Component in column 2, row 1 position (index 9)
    * @param m22 Component in column 2, row 2 position (index 10)
    * @param m23 Component in column 2, row 3 position (index 11)
    * @param m30 Component in column 3, row 0 position (index 12)
    * @param m31 Component in column 3, row 1 position (index 13)
    * @param m32 Component in column 3, row 2 position (index 14)
    * @param m33 Component in column 3, row 3 position (index 15)
    * @returns A new Mat4
    */
    public static function fromValues( ?out : Mat4, m00 : Float, m01 : Float, m02 : Float, m03 : Float, m10 : Float, m11 : Float, m12 : Float, m13 : Float, m20 : Float, m21 : Float, m22 : Float, m23 : Float, m30 : Float, m31 : Float, m32 : Float, m33 : Float ) : Mat4 {
        if( out == null ) out = Mat4Tools.create();
        out[0] = m00;
        out[1] = m01;
        out[2] = m02;
        out[3] = m03;
        out[4] = m10;
        out[5] = m11;
        out[6] = m12;
        out[7] = m13;
        out[8] = m20;
        out[9] = m21;
        out[10] = m22;
        out[11] = m23;
        out[12] = m30;
        out[13] = m31;
        out[14] = m32;
        out[15] = m33;
        return out;
    }

    /**
    * Creates a matrix from a vector translation
    * This is equivalent to (but much faster than):
    *
    *     Mat4.identity(dest);
    *     Mat4.translate(dest, dest, vec);
    *
    * @param out Mat4 receiving operation result
    * @param v Translation vector
    * @returns out
    */
    public static function fromTranslation( ?out : Mat4, v : Vec3 ) : Mat4 {
        if( out == null ) out = Mat4Tools.create();
        out[0] = 1;
        out[1] = 0;
        out[2] = 0;
        out[3] = 0;
        out[4] = 0;
        out[5] = 1;
        out[6] = 0;
        out[7] = 0;
        out[8] = 0;
        out[9] = 0;
        out[10] = 1;
        out[11] = 0;
        out[12] = v[0];
        out[13] = v[1];
        out[14] = v[2];
        out[15] = 1;
        return out;
    }

    /**
    * Creates a matrix from a vector scaling
    * This is equivalent to (but much faster than):
    *
    *     Mat4.identity(dest);
    *     Mat4.scale(dest, dest, vec);
    *
    * @param out Mat4 receiving operation result
    * @param v Scaling vector
    * @returns out
    */
    public static function fromScaling( ?out : Mat4, v : Vec3 ) : Mat4 {
        if( out == null ) out = Mat4Tools.create();
        out[0] = v[0];
        out[1] = 0;
        out[2] = 0;
        out[3] = 0;
        out[4] = 0;
        out[5] = v[1];
        out[6] = 0;
        out[7] = 0;
        out[8] = 0;
        out[9] = 0;
        out[10] = v[2];
        out[11] = 0;
        out[12] = 0;
        out[13] = 0;
        out[14] = 0;
        out[15] = 1;
        return out;
    }

    /**
    * Creates a matrix from a given angle around a given axis
    * This is equivalent to (but much faster than):
    *
    *     Mat4.identity(dest);
    *     Mat4.rotate(dest, dest, rad, axis);
    *
    * @param out Mat4 receiving operation result
    * @param rad the angle to rotate the matrix by
    * @param axis the axis to rotate around
    * @returns out
    */
    public static function fromRotation( ?out : Mat4, rad : Float, axis : Vec3 ) : Mat4 {
        if( out == null ) out = Mat4Tools.create();
        var x = axis[0], y = axis[1], z = axis[2];
        var len = Math.sqrt(x * x + y * y + z * z);
        var s, c, t;

        if (len < GLMatrix.EPSILON) { return null; }

        len = 1 / len;
        x *= len;
        y *= len;
        z *= len;

        s = Math.sin(rad);
        c = Math.cos(rad);
        t = 1 - c;

        // Perform rotation-specific matrix multiplication
        out[0] = x * x * t + c;
        out[1] = y * x * t + z * s;
        out[2] = z * x * t - y * s;
        out[3] = 0;
        out[4] = x * y * t - z * s;
        out[5] = y * y * t + c;
        out[6] = z * y * t + x * s;
        out[7] = 0;
        out[8] = x * z * t + y * s;
        out[9] = y * z * t - x * s;
        out[10] = z * z * t + c;
        out[11] = 0;
        out[12] = 0;
        out[13] = 0;
        out[14] = 0;
        out[15] = 1;
        return out;
    }

    /**
    * Creates a matrix from the given angle around the X axis
    * This is equivalent to (but much faster than):
    *
    *     Mat4.identity(dest);
    *     Mat4.rotateX(dest, dest, rad);
    *
    * @param out Mat4 receiving operation result
    * @param rad the angle to rotate the matrix by
    * @returns out
    */
    public static function fromXRotation( ?out : Mat4, rad : Float ) : Mat4 {
        if( out == null ) out = Mat4Tools.create();
        var s = Math.sin(rad);
        var c = Math.cos(rad);

        // Perform axis-specific matrix multiplication
        out[0]  = 1;
        out[1]  = 0;
        out[2]  = 0;
        out[3]  = 0;
        out[4] = 0;
        out[5] = c;
        out[6] = s;
        out[7] = 0;
        out[8] = 0;
        out[9] = -s;
        out[10] = c;
        out[11] = 0;
        out[12] = 0;
        out[13] = 0;
        out[14] = 0;
        out[15] = 1;
        return out;
    }

    /**
    * Creates a matrix from the given angle around the Y axis
    * This is equivalent to (but much faster than):
    *
    *     Mat4.identity(dest);
    *     Mat4.rotateY(dest, dest, rad);
    *
    * @param out Mat4 receiving operation result
    * @param rad the angle to rotate the matrix by
    * @returns out
    */
    public static function fromYRotation( ?out : Mat4, rad : Float ) : Mat4 {
        if( out == null ) out = Mat4Tools.create();
        var s = Math.sin(rad);
        var c = Math.cos(rad);

        // Perform axis-specific matrix multiplication
        out[0]  = c;
        out[1]  = 0;
        out[2]  = -s;
        out[3]  = 0;
        out[4] = 0;
        out[5] = 1;
        out[6] = 0;
        out[7] = 0;
        out[8] = s;
        out[9] = 0;
        out[10] = c;
        out[11] = 0;
        out[12] = 0;
        out[13] = 0;
        out[14] = 0;
        out[15] = 1;
        return out;
    }

    /**
    * Creates a matrix from the given angle around the Z axis
    * This is equivalent to (but much faster than):
    *
    *     Mat4.identity(dest);
    *     Mat4.rotateZ(dest, dest, rad);
    *
    * @param out Mat4 receiving operation result
    * @param rad the angle to rotate the matrix by
    * @returns out
    */
    public static function fromZRotation( ?out : Mat4, rad : Float ) : Mat4 {
        if( out == null ) out = Mat4Tools.create();
        var s = Math.sin(rad);
        var c = Math.cos(rad);

        // Perform axis-specific matrix multiplication
        out[0]  = c;
        out[1]  = s;
        out[2]  = 0;
        out[3]  = 0;
        out[4] = -s;
        out[5] = c;
        out[6] = 0;
        out[7] = 0;
        out[8] = 0;
        out[9] = 0;
        out[10] = 1;
        out[11] = 0;
        out[12] = 0;
        out[13] = 0;
        out[14] = 0;
        out[15] = 1;
        return out;
    }

    /**
    * Creates a matrix from a quaternion rotation and vector translation
    * This is equivalent to (but much faster than):
    *
    *     Mat4.identity(dest);
    *     Mat4.translate(dest, vec);
    *     var quatMat = Mat4.create();
    *     quat4.toMat4(quat, quatMat);
    *     Mat4.multiply(dest, quatMat);
    *
    * @param out Mat4 receiving operation result
    * @param q Rotation quaternion
    * @param v Translation vector
    * @returns out
    */
    public static function fromRotationTranslation( ?out : Mat4, q : Quat, v : Vec3 ) : Mat4 {
        if( out == null ) out = Mat4Tools.create();
        // Quaternion math
        var x = q[0], y = q[1], z = q[2], w = q[3];
        var x2 = x + x;
        var y2 = y + y;
        var z2 = z + z;

        var xx = x * x2;
        var xy = x * y2;
        var xz = x * z2;
        var yy = y * y2;
        var yz = y * z2;
        var zz = z * z2;
        var wx = w * x2;
        var wy = w * y2;
        var wz = w * z2;

        out[0] = 1 - (yy + zz);
        out[1] = xy + wz;
        out[2] = xz - wy;
        out[3] = 0;
        out[4] = xy - wz;
        out[5] = 1 - (xx + zz);
        out[6] = yz + wx;
        out[7] = 0;
        out[8] = xz + wy;
        out[9] = yz - wx;
        out[10] = 1 - (xx + yy);
        out[11] = 0;
        out[12] = v[0];
        out[13] = v[1];
        out[14] = v[2];
        out[15] = 1;

        return out;
    }

    /**
    * Creates a new Mat4 from a dual quat.
    *
    * @param out Matrix
    * @param a Dual Quaternion
    * @returns Mat4 receiving operation result
    */
    public static function fromQuat2( ?out : Mat4, a : Quat2 ) : Mat4 {
        if( out == null ) out = Mat4Tools.create();
        var translation = new Vec3();
        var bx = -a[0], by = -a[1], bz = -a[2], bw = a[3],
        ax = a[4], ay = a[5], az = a[6], aw = a[7];

        var magnitude = bx * bx + by * by + bz * bz + bw * bw;
        //Only scale if it makes sense
        if (magnitude > 0) {
            translation[0] = (ax * bw + aw * bx + ay * bz - az * by) * 2 / magnitude;
            translation[1] = (ay * bw + aw * by + az * bx - ax * bz) * 2 / magnitude;
            translation[2] = (az * bw + aw * bz + ax * by - ay * bx) * 2 / magnitude;
        } else {
            translation[0] = (ax * bw + aw * bx + ay * bz - az * by) * 2;
            translation[1] = (ay * bw + aw * by + az * bx - ax * bz) * 2;
            translation[2] = (az * bw + aw * bz + ax * by - ay * bx) * 2;
        }
        var a_r = QuatTools.create();
        Quat2Tools.getReal( a, a_r );
        fromRotationTranslation(out, a_r, translation);

        return out;
    }

    /**
    * Creates a matrix from a quaternion rotation, vector translation and vector scale
    * This is equivalent to (but much faster than):
    *
    *     Mat4.identity(dest);
    *     Mat4.translate(dest, vec);
    *     var quatMat = Mat4.create();
    *     quat4.toMat4(quat, quatMat);
    *     Mat4.multiply(dest, quatMat);
    *     Mat4.scale(dest, scale)
    *
    * @param out Mat4 receiving operation result
    * @param q Rotation quaternion
    * @param v Translation vector
    * @param s Scaling vector
    * @returns out
    */
    public static function fromRotationTranslationScale( ?out : Mat4, q : Quat, v : Vec3, s : Vec3 ) : Mat4 {
        if( out == null ) out = Mat4Tools.create();
        // Quaternion math
        var x = q[0], y = q[1], z = q[2], w = q[3];
        var x2 = x + x;
        var y2 = y + y;
        var z2 = z + z;

        var xx = x * x2;
        var xy = x * y2;
        var xz = x * z2;
        var yy = y * y2;
        var yz = y * z2;
        var zz = z * z2;
        var wx = w * x2;
        var wy = w * y2;
        var wz = w * z2;
        var sx = s[0];
        var sy = s[1];
        var sz = s[2];

        out[0] = (1 - (yy + zz)) * sx;
        out[1] = (xy + wz) * sx;
        out[2] = (xz - wy) * sx;
        out[3] = 0;
        out[4] = (xy - wz) * sy;
        out[5] = (1 - (xx + zz)) * sy;
        out[6] = (yz + wx) * sy;
        out[7] = 0;
        out[8] = (xz + wy) * sz;
        out[9] = (yz - wx) * sz;
        out[10] = (1 - (xx + yy)) * sz;
        out[11] = 0;
        out[12] = v[0];
        out[13] = v[1];
        out[14] = v[2];
        out[15] = 1;

        return out;
    }

    /**
    * Creates a matrix from a quaternion rotation, vector translation and vector scale, rotating and scaling around the given origin
    * This is equivalent to (but much faster than):
    *
    *     Mat4.identity(dest);
    *     Mat4.translate(dest, vec);
    *     Mat4.translate(dest, origin);
    *     var quatMat = Mat4.create();
    *     quat4.toMat4(quat, quatMat);
    *     Mat4.multiply(dest, quatMat);
    *     Mat4.scale(dest, scale)
    *     Mat4.translate(dest, negativeOrigin);
    *
    * @param out Mat4 receiving operation result
    * @param q Rotation quaternion
    * @param v Translation vector
    * @param s Scaling vector
    * @param o The origin vector around which to scale and rotate
    * @returns out
    */
    public static function fromRotationTranslationScaleOrigin( ?out : Mat4, q : Quat, v : Vec3, s : Vec3, o : Vec3 ) : Mat4 {
        if( out == null ) out = Mat4Tools.create();
        // Quaternion math
        var x = q[0], y = q[1], z = q[2], w = q[3];
        var x2 = x + x;
        var y2 = y + y;
        var z2 = z + z;

        var xx = x * x2;
        var xy = x * y2;
        var xz = x * z2;
        var yy = y * y2;
        var yz = y * z2;
        var zz = z * z2;
        var wx = w * x2;
        var wy = w * y2;
        var wz = w * z2;

        var sx = s[0];
        var sy = s[1];
        var sz = s[2];

        var ox = o[0];
        var oy = o[1];
        var oz = o[2];

        var out0 = (1 - (yy + zz)) * sx;
        var out1 = (xy + wz) * sx;
        var out2 = (xz - wy) * sx;
        var out4 = (xy - wz) * sy;
        var out5 = (1 - (xx + zz)) * sy;
        var out6 = (yz + wx) * sy;
        var out8 = (xz + wy) * sz;
        var out9 = (yz - wx) * sz;
        var out10 = (1 - (xx + yy)) * sz;

        out[0] = out0;
        out[1] = out1;
        out[2] = out2;
        out[3] = 0;
        out[4] = out4;
        out[5] = out5;
        out[6] = out6;
        out[7] = 0;
        out[8] = out8;
        out[9] = out9;
        out[10] = out10;
        out[11] = 0;
        out[12] = v[0] + ox - (out0 * ox + out4 * oy + out8 * oz);
        out[13] = v[1] + oy - (out1 * ox + out5 * oy + out9 * oz);
        out[14] = v[2] + oz - (out2 * ox + out6 * oy + out10 * oz);
        out[15] = 1;

        return out;
    }

    /**
    * Calculates a 4x4 matrix from the given quaternion
    *
    * @param out Mat4 receiving operation result
    * @param q Quaternion to create matrix from
    *
    * @returns out
    */
    public static function fromQuat( ?out : Mat4, q : Quat ) : Mat4 {
        if( out == null ) out = Mat4Tools.create();
        var x = q[0], y = q[1], z = q[2], w = q[3];
        var x2 = x + x;
        var y2 = y + y;
        var z2 = z + z;

        var xx = x * x2;
        var yx = y * x2;
        var yy = y * y2;
        var zx = z * x2;
        var zy = z * y2;
        var zz = z * z2;
        var wx = w * x2;
        var wy = w * y2;
        var wz = w * z2;

        out[0] = 1 - yy - zz;
        out[1] = yx + wz;
        out[2] = zx - wy;
        out[3] = 0;

        out[4] = yx - wz;
        out[5] = 1 - xx - zz;
        out[6] = zy + wx;
        out[7] = 0;

        out[8] = zx + wy;
        out[9] = zy - wx;
        out[10] = 1 - xx - yy;
        out[11] = 0;

        out[12] = 0;
        out[13] = 0;
        out[14] = 0;
        out[15] = 1;

        return out;
    }

    /**
    * Copies Mat3 into the upper-left 3x3 values of the given Mat4.
    * 
    *
    * @param out the receiving 4x4 matrix
    * @param a   the source 3x3 matrix
    * @returns out
    */
    public static function fromMat3( ?out : Mat4, a : Mat3 ) : Mat4 {
        if( out == null ) out = Mat4Tools.create();
        out[0] = a[0];
        out[1] = a[1];
        out[2] = a[2];
        out[4] = a[3];
        out[5] = a[4];
        out[6] = a[5];
        out[8] = a[6];
        out[9] = a[7];
        out[10] = a[8];
        return out;
    }
}
