package mme.math.glmatrix;

import mme.math.glmatrix.GLMatrix;

import mme.math.glmatrix.Vec2;
import mme.math.glmatrix.Vec3;
import mme.math.glmatrix.Mat2;
import mme.math.glmatrix.Mat2d;
import mme.math.glmatrix.Mat3;
import mme.math.glmatrix.Mat4;

/**
 * 2 Dimensional Vector
 * @module Vec2
 */
class Vec2Tools {

/*

//
// GENERATORS in Vec2 class
//

Vec2.fromValues( ?out : Vec2, x : Float, y : Float ) : Vec2;


//
// GENERATORS
//
create() : Vec2;
vec2.clone( a : Vec2 ) : Vec2;

?vec2.random( ?out : Vec2, scale : Float = 1.0 ) : Vec2;

lerp( t : Float, a : Vec2, b : Vec2, ?out : Vec2 ) : Vec2;


//
// EDIT
//
vec2.set( out : Vec2, x : Float, y : Float ) : Vec2;
vec2.zero( out : Vec2 ) : Vec2;


//
// OPERATORS
//

// unary
//
vec2.copy( a : Vec2, ?out : Vec2 ) : Vec2;
vec2.ceil( a : Vec2, ?out : Vec2 ) : Vec2;
vec2.floor( a : Vec2, ?out : Vec2 ) : Vec2;
vec2.round ( a : Vec2, ?out : Vec2 ) : Vec2;
vec2.negate( a : Vec2, ?out : Vec2 ) : Vec2;
vec2.inverse( a : Vec2, ?out : Vec2 ) : Vec2;
vec2.normalize( a : Vec2, ?out : Vec2 ) : Vec2;

// unary, non-Vec2 result
//
vec2.length( a : Vec2 ) : Float;
vec2.squaredLength ( a : Vec2 ) : Float;
vec2.str( a : Vec2 ) : String;


// binary
//
vec2.add( a : Vec2, b : Vec2, ?out : Vec2 ) : Vec2;
vec2.subtract( a : Vec2, b : Vec2, ?out : Vec2 ) : Vec2;
vec2.multiply( a : Vec2, b : Vec2, ?out : Vec2 ) : Vec2;
vec2.divide( a : Vec2, b : Vec2, ?out : Vec2 ) : Vec2;
vec2.min( a : Vec2, b : Vec2, ?out : Vec2 ) : Vec2;
vec2.max( a : Vec2, b : Vec2, ?out : Vec2 ) : Vec2;

// binary, non-Vec2 arg
//
vec2.scale( a : Vec2, b : Float, ?out : Vec2 ) : Vec2;
vec2.postmultiplyMat2( a : Vec2, m : Mat2, ?out : Vec2 ) : Vec2; // a.k.a Mat2Tools.multiplyVec2()
vec2.postmultiplyMat2d( a : Vec2, m : Mat2d, ?out : Vec2 ) : Vec2; // a.k.a Mat2dTools.multiplyVec2()
vec2.postmultiplyMat3( a : Vec2, m : Mat3, ?out : Vec2 ) : Vec2; // a.k.a Mat3Tools.multiplyVec2()
vec2.postmultiplyMat4( a : Vec2, m : Mat4, ?out : Vec2 ) : Vec2; // a.k.a Mat4Tools.multiplyVec2()

// binary, non-Vec2 result
//
vec2.dot( a : Vec2, b : Vec2 ) : Float;
vec2.cross( a : Vec2, b : Vec2, ?out : Vec3 ) : Vec3;
vec2.angle( a : Vec2, b : Vec2 ) : Float;
vec2.exactEquals( a : Vec2, b : Vec2 ) : Bool;
vec2.equals( a : Vec2, b : Vec2 ) : Bool;
vec2.distance( a : Vec2, b : Vec2 ) : Float;
vec2.squaredDistance( a : Vec2, b : Vec2 ) : Float;


// ternary
//
vec2.scaleAndAdd( a : Vec2, b : Vec2, scale : Float, ?out : Vec2 ) : Vec2;
vec2.rotate( a : Vec2, b : Vec2, c : Float, ?out : Vec2 ) : Vec2;

*/


    /**
    * Creates a new, empty Vec2
    *
    * @returns {Vec2} a new 2D vector
    */
    public static inline function create() : Vec2 {
        var out = new Vec2();
        return out;
    }

    /**
    * Creates a new Vec2 initialized with values from an existing vector
    *
    * @param {Vec2} a vector to clone
    * @returns {Vec2} a new 2D vector
    */
    public static function clone( a : Vec2 ) : Vec2 {
        var out = new Vec2();
        out[0] = a[0];
        out[1] = a[1];
        return out;
    }

    /**
    * Copy the values from one Vec2 to another
    *
    * @param {Vec2} out the receiving vector
    * @param {Vec2} a the source vector
    * @returns {Vec2} out
    */
    public static function copy( a : Vec2, ?out : Vec2 ) : Vec2 {
    if( out == null ) out = create();
    out[0] = a[0];
    out[1] = a[1];
    return out;
    }

    /**
    * Set the components of a Vec2 to the given values
    *
    * @param {Vec2} out the receiving vector
    * @param {Float} x X component
    * @param {Float} y Y component
    * @returns {Vec2} out
    */
    public static function set( out : Vec2, x : Float, y : Float ) : Vec2 {
    out[0] = x;
    out[1] = y;
    return out;
    }

    /**
    * Adds two Vec2's
    *
    * @param {Vec2} out the receiving vector
    * @param {Vec2} a the first operand
    * @param {Vec2} b the second operand
    * @returns {Vec2} out
    */
    public static function add( a : Vec2, b : Vec2, ?out : Vec2 ) : Vec2 {
    if( out == null ) out = create();
    out[0] = a[0] + b[0];
    out[1] = a[1] + b[1];
    return out;
    }

    /**
    * Subtracts vector b from vector a
    *
    * @param {Vec2} out the receiving vector
    * @param {Vec2} a the first operand
    * @param {Vec2} b the second operand
    * @returns {Vec2} out
    */
    public static function subtract( a : Vec2, b : Vec2, ?out : Vec2 ) : Vec2 {
    if( out == null ) out = create();
    out[0] = a[0] - b[0];
    out[1] = a[1] - b[1];
    return out;
    }

    /**
    * Multiplies two Vec2's
    *
    * @param {Vec2} out the receiving vector
    * @param {Vec2} a the first operand
    * @param {Vec2} b the second operand
    * @returns {Vec2} out
    */
    public static function multiply( a : Vec2, b : Vec2, ?out : Vec2 ) : Vec2 {
    if( out == null ) out = create();
    out[0] = a[0] * b[0];
    out[1] = a[1] * b[1];
    return out;
    }

    /**
    * Divides two Vec2's
    *
    * @param {Vec2} out the receiving vector
    * @param {Vec2} a the first operand
    * @param {Vec2} b the second operand
    * @returns {Vec2} out
    */
    public static function divide( a : Vec2, b : Vec2, ?out : Vec2 ) : Vec2 {
    if( out == null ) out = create();
    out[0] = a[0] / b[0];
    out[1] = a[1] / b[1];
    return out;
    }

    /**
    * Math.ceil the components of a Vec2
    *
    * @param {Vec2} out the receiving vector
    * @param {Vec2} a vector to ceil
    * @returns {Vec2} out
    */
    public static function ceil( a : Vec2, ?out : Vec2 ) : Vec2 {
    if( out == null ) out = create();
    out[0] = Math.ceil(a[0]);
    out[1] = Math.ceil(a[1]);
    return out;
    }

    /**
    * Math.floor the components of a Vec2
    *
    * @param {Vec2} out the receiving vector
    * @param {Vec2} a vector to floor
    * @returns {Vec2} out
    */
    public static function floor( a : Vec2, ?out : Vec2 ) : Vec2 {
    if( out == null ) out = create();
    out[0] = Math.floor(a[0]);
    out[1] = Math.floor(a[1]);
    return out;
    }

    /**
    * Returns the minimum of two Vec2's
    *
    * @param {Vec2} out the receiving vector
    * @param {Vec2} a the first operand
    * @param {Vec2} b the second operand
    * @returns {Vec2} out
    */
    public static function min( a : Vec2, b : Vec2, ?out : Vec2 ) : Vec2 {
    if( out == null ) out = create();
    out[0] = Math.min(a[0], b[0]);
    out[1] = Math.min(a[1], b[1]);
    return out;
    }

    /**
    * Returns the maximum of two Vec2's
    *
    * @param {Vec2} out the receiving vector
    * @param {Vec2} a the first operand
    * @param {Vec2} b the second operand
    * @returns {Vec2} out
    */
    public static function max( a : Vec2, b : Vec2, ?out : Vec2 ) : Vec2 {
    if( out == null ) out = create();
    out[0] = Math.max(a[0], b[0]);
    out[1] = Math.max(a[1], b[1]);
    return out;
    }

    /**
    * Math.round the components of a Vec2
    *
    * @param {Vec2} out the receiving vector
    * @param {Vec2} a vector to round
    * @returns {Vec2} out
    */
    public static function round ( a : Vec2, ?out : Vec2 ) : Vec2 {
    if( out == null ) out = create();
    out[0] = Math.round(a[0]);
    out[1] = Math.round(a[1]);
    return out;
    }

    /**
    * Scales a Vec2 by a scalar number
    *
    * @param {Vec2} out the receiving vector
    * @param {Vec2} a the vector to scale
    * @param {Float} b amount to scale the vector by
    * @returns {Vec2} out
    */
    public static function scale( a : Vec2, b : Float, ?out : Vec2 ) : Vec2 {
    if( out == null ) out = create();
    out[0] = a[0] * b;
    out[1] = a[1] * b;
    return out;
    }

    /**
    * Adds two Vec2's after scaling the second operand by a scalar value
    *
    * @param {Vec2} out the receiving vector
    * @param {Vec2} a the first operand
    * @param {Vec2} b the second operand
    * @param {Float} scale the amount to scale b by before adding
    * @returns {Vec2} out
    */
    public static function scaleAndAdd( a : Vec2, b : Vec2, scale : Float, ?out : Vec2 ) : Vec2 {
    if( out == null ) out = create();
    out[0] = a[0] + (b[0] * scale);
    out[1] = a[1] + (b[1] * scale);
    return out;
    }

    /**
    * Calculates the euclidian distance between two Vec2's
    *
    * @param {Vec2} a the first operand
    * @param {Vec2} b the second operand
    * @returns {Float} distance between a and b
    */
    public static function distance( a : Vec2, b : Vec2 ) : Float {
    var x = b[0] - a[0],
        y = b[1] - a[1];
    return Math.sqrt(x*x + y*y);
    }

    /**
    * Calculates the squared euclidian distance between two Vec2's
    *
    * @param {Vec2} a the first operand
    * @param {Vec2} b the second operand
    * @returns {Float} squared distance between a and b
    */
    public static function squaredDistance( a : Vec2, b : Vec2 ) : Float {
    var x = b[0] - a[0],
        y = b[1] - a[1];
    return x*x + y*y;
    }

    /**
    * Calculates the length of a Vec2
    *
    * @param {Vec2} a vector to calculate length of
    * @returns {Float} length of a
    */
    public static function length( a : Vec2 ) : Float {
    var x = a[0],
        y = a[1];
    return Math.sqrt(x*x + y*y);
    }

    /**
    * Calculates the squared length of a Vec2
    *
    * @param {Vec2} a vector to calculate squared length of
    * @returns {Float} squared length of a
    */
    public static function squaredLength ( a : Vec2 ) : Float {
    var x = a[0],
        y = a[1];
    return x*x + y*y;
    }

    /**
    * Negates the components of a Vec2
    *
    * @param {Vec2} out the receiving vector
    * @param {Vec2} a vector to negate
    * @returns {Vec2} out
    */
    public static function negate( a : Vec2, ?out : Vec2 ) : Vec2 {
    if( out == null ) out = create();
    out[0] = -a[0];
    out[1] = -a[1];
    return out;
    }

    /**
    * Returns the inverse of the components of a Vec2
    *
    * @param {Vec2} out the receiving vector
    * @param {Vec2} a vector to invert
    * @returns {Vec2} out
    */
    public static function inverse( a : Vec2, ?out : Vec2 ) : Vec2 {
    if( out == null ) out = create();
    out[0] = 1.0 / a[0];
    out[1] = 1.0 / a[1];
    return out;
    }

    /**
    * Normalize a Vec2
    *
    * @param {Vec2} out the receiving vector
    * @param {Vec2} a vector to normalize
    * @returns {Vec2} out
    */
    public static function normalize( a : Vec2, ?out : Vec2 ) : Vec2 {
    if( out == null ) out = create();
    var x = a[0],
        y = a[1];
    var len = x*x + y*y;
    if (len > 0) {
        //TODO: evaluate use of glm_invsqrt here?
        len = 1 / Math.sqrt(len);
    }    
    out[0] = a[0] * len;
    out[1] = a[1] * len;
    return out;
    }

    /**
    * Calculates the dot product of two Vec2's
    *
    * @param {Vec2} a the first operand
    * @param {Vec2} b the second operand
    * @returns {Float} dot product of a and b
    */
    public static function dot( a : Vec2, b : Vec2 ) : Float {
    return a[0] * b[0] + a[1] * b[1];
    }

    /**
    * Computes the cross product of two Vec2's
    * Note that the cross product must by definition produce a 3D vector
    *
    * @param {Vec3} out the receiving vector
    * @param {Vec2} a the first operand
    * @param {Vec2} b the second operand
    * @returns {Vec3} out
    */
    public static function cross( a : Vec2, b : Vec2, ?out : Vec3 ) : Vec3 {
    if( out == null ) out = new Vec3();
    var z = a[0] * b[1] - a[1] * b[0];
    out[0] = out[1] = 0;
    out[2] = z;
    return out;
    }

    /**
    * Performs a linear interpolation between two Vec2's
    *
    * @param {Vec2} out the receiving vector
    * @param {Vec2} a the first operand
    * @param {Vec2} b the second operand
    * @param {Float} t interpolation amount, in the range [0-1], between the two inputs
    * @returns {Vec2} out
    */
    public static function lerp( t : Float, a : Vec2, b : Vec2, ?out : Vec2 ) : Vec2 {
    if( out == null ) out = create();
    var ax = a[0],
        ay = a[1];
    out[0] = ax + t * (b[0] - ax);
    out[1] = ay + t * (b[1] - ay);
    return out;
    }

    /**
    * Generates a random vector with the given scale
    *
    * @param {Vec2} out the receiving vector
    * @param {Float} [scale] Length of the resulting vector. If ommitted, a unit vector will be returned
    * @returns {Vec2} out
    */
    public static function random( ?out : Vec2, scale : Float = 1.0 ) : Vec2 {
    if( out == null ) out = create();
    var r = GLMatrix.RANDOM() * 2.0 * Math.PI;
    out[0] = Math.cos(r) * scale;
    out[1] = Math.sin(r) * scale;
    return out;
    }

    /**
    * Transforms the Vec2 with a Mat2
    * out = m * a
    *
    * @param {Vec2} out the receiving vector
    * @param {Vec2} a the vector to transform
    * @param {Mat2} m matrix to transform with
    * @returns {Vec2} out
    */
    public static function postmultiplyMat2( a : Vec2, m : Mat2, ?out : Vec2 ) : Vec2 {
    if( out == null ) out = create();
    var x = a[0],
        y = a[1];
    out[0] = m[0] * x + m[2] * y;
    out[1] = m[1] * x + m[3] * y;
    return out;
    }

    /**
    * Transforms the Vec2 with a Mat2d
    * out = m * a
    *
    * @param {Vec2} out the receiving vector
    * @param {Vec2} a the vector to transform
    * @param {Mat2d} m matrix to transform with
    * @returns {Vec2} out
    */
    public static function postmultiplyMat2d( a : Vec2, m : Mat2d, ?out : Vec2 ) : Vec2 {
    if( out == null ) out = create();
    var x = a[0],
        y = a[1];
    out[0] = m[0] * x + m[2] * y + m[4];
    out[1] = m[1] * x + m[3] * y + m[5];
    return out;
    }

    /**
    * Transforms the Vec2 with a Mat3
    * 3rd vector component is implicitly '1'
    * out = m * a
    *
    * @param {Vec2} out the receiving vector
    * @param {Vec2} a the vector to transform
    * @param {Mat3} m matrix to transform with
    * @returns {Vec2} out
    */
    public static function postmultiplyMat3( a : Vec2, m : Mat3, ?out : Vec2 ) : Vec2 {
    if( out == null ) out = create();
    var x = a[0],
        y = a[1];
    out[0] = m[0] * x + m[3] * y + m[6];
    out[1] = m[1] * x + m[4] * y + m[7];
    return out;
    }

    /**
    * Transforms the Vec2 with a mat4
    * 3rd vector component is implicitly '0'
    * 4th vector component is implicitly '1'
    *
    * @param {Vec2} out the receiving vector
    * @param {Vec2} a the vector to transform
    * @param {mat4} m matrix to transform with
    * @returns {Vec2} out
    */
    public static function postmultiplyMat4( a : Vec2, m : Mat4, ?out : Vec2 ) : Vec2 {
    if( out == null ) out = create();
    var x = a[0];
    var y = a[1];
    out[0] = m[0] * x + m[4] * y + m[12];
    out[1] = m[1] * x + m[5] * y + m[13];
    return out;
    }

    /**
    * Rotate a 2D vector
    * @param {Vec2} out The receiving Vec2
    * @param {Vec2} a The Vec2 point to rotate
    * @param {Vec2} b The origin of the rotation
    * @param {Float} c The angle of rotation
    * @returns {Vec2} out
    */
    public static function rotate( a : Vec2, ?b : Vec2, c : Float, ?out : Vec2 ) : Vec2 {
    if( out == null ) out = create();
    if( b == null ) b = [0,0];
    //Translate point to the origin
    var p0 = a[0] - b[0],
    p1 = a[1] - b[1],
    sinC = Math.sin(c),
    cosC = Math.cos(c);
    
    //perform rotation and translate to correct position
    out[0] = p0*cosC - p1*sinC + b[0];
    out[1] = p0*sinC + p1*cosC + b[1];

    return out;
    }

    /**
    * Get the angle between two 2D vectors
    * @param {Vec2} a The first operand
    * @param {Vec2} b The second operand
    * @returns {Float} The angle in radians
    */
    public static function angle( a : Vec2, b : Vec2 ) : Float {
    var x1 = a[0],
        y1 = a[1],
        x2 = b[0],
        y2 = b[1];
    
    var len1 = x1*x1 + y1*y1;
    if (len1 > 0) {
        len1 = 1 / Math.sqrt(len1);
    }
    
    var len2 = x2*x2 + y2*y2;
    if (len2 > 0) {
        len2 = 1 / Math.sqrt(len2);
    }
    
    var cosine = (x1 * x2 + y1 * y2) * len1 * len2;
    
    
    if(cosine > 1.0) {
        return 0;
    }
    else if(cosine < -1.0) {
        return Math.PI;
    } else {
        return Math.acos(cosine);
    }
    }

    /**
    * Set the components of a Vec2 to zero
    *
    * @param {Vec2} out the receiving vector
    * @returns {Vec2} out
    */
    public static function zero( out : Vec2 ) : Vec2 {
    out[0] = 0.0;
    out[1] = 0.0;
    return out;
    }

    /**
    * Returns a string representation of a vector
    *
    * @param {Vec2} a vector to represent as a string
    * @returns {String} string representation of the vector
    */
    public static function str( a : Vec2 ) : String {
    return 'Vec2(' + a[0] + ', ' + a[1] + ')';
    }

    /**
    * Returns whether or not the vectors exactly have the same elements in the same position (when compared with ==)
    *
    * @param {Vec2} a The first vector.
    * @param {Vec2} b The second vector.
    * @returns {Boolean} True if the vectors are equal, false otherwise.
    */
    public static function exactEquals( a : Vec2, b : Vec2 ) : Bool {
    return a[0] == b[0] && a[1] == b[1];
    }

    /**
    * Returns whether or not the vectors have approximately the same elements in the same position.
    *
    * @param {Vec2} a The first vector.
    * @param {Vec2} b The second vector.
    * @returns {Boolean} True if the vectors are equal, false otherwise.
    */
    public static function equals( a : Vec2, b : Vec2 ) : Bool {
    var a0 = a[0], a1 = a[1];
    var b0 = b[0], b1 = b[1];
    return (Math.abs(a0 - b0) <= GLMatrix.EPSILON*Math.max(1.0, Math.max(Math.abs(a0), Math.abs(b0))) &&
            Math.abs(a1 - b1) <= GLMatrix.EPSILON*Math.max(1.0, Math.max(Math.abs(a1), Math.abs(b1))));
    }


    /**
    * Alias for {@link Vec2.length}
    * @function
    */
    public static var len = length;

    /**
    * Alias for {@link Vec2.subtract}
    * @function
    */
    public static var sub = subtract;

    /**
    * Alias for {@link Vec2.multiply}
    * @function
    */
    public static var mul = multiply;

    /**
    * Alias for {@link Vec2.divide}
    * @function
    */
    public static var div = divide;

    /**
    * Alias for {@link Vec2.distance}
    * @function
    */
    public static var dist = distance;

    /**
    * Alias for {@link Vec2.squaredDistance}
    * @function
    */
    public static var sqrDist = squaredDistance;

    /**
    * Alias for {@link Vec2.squaredLength}
    * @function
    */
    public static var sqrLen = squaredLength;

}
