package mme.math.glmatrix;

import mme.math.glmatrix.Mat2dTools;

#if lime
import lime.utils.Float32Array;
abstract Mat2d(Float32Array) from Float32Array to Float32Array {
#else
import haxe.ds.Vector;
abstract Mat2d(Vector<Float>) from Vector<Float> to Vector<Float> {
#end
    public inline function new() {
        #if lime
        this = new Float32Array(6);
        #else
        this = new Vector<Float>(6);
        this[1] = 0.0;
        this[2] = 0.0;
        this[4] = 0.0;
        this[5] = 0.0;
        #end
        this[0] = 1.0;
        this[3] = 1.0;
    }
    @:arrayAccess
    public inline function getk(key:Int) {
    return this[key];
    }
    @:arrayAccess
    public inline function setk(k:Int, v:Float):Float {
        this[k] = v;
        return v;
    }
    @:from
    static public function fromArray(a:Array<Float>) {
        return fromValues( a[0], a[1], a[2], a[3], a[4], a[5] );
    }
    @:to
    public function toArray() {
        return [this[0], this[1], this[2], this[3], this[4], this[5]];
    }
    public function toString() : String {
        return Mat2dTools.str(this);
    }

    /**
    * Create a new Mat2d with the given values
    *
    * @param a Component A (index 0)
    * @param b Component B (index 1)
    * @param c Component C (index 2)
    * @param d Component D (index 3)
    * @param tx Component TX (index 4)
    * @param ty Component TY (index 5)
    * @returns A new Mat2d
    */
    public static function fromValues( ?out : Mat2d, a : Float, b : Float, c : Float, d : Float, tx : Float, ty : Float ) : Mat2d {
        if( out == null ) out = Mat2dTools.create();
        out[0] = a;
        out[1] = b;
        out[2] = c;
        out[3] = d;
        out[4] = tx;
        out[5] = ty;
        return out;
    }

    /**
    * Creates a matrix from a given angle
    * This is equivalent to (but much faster than):
    *
    *     Mat2d.identity(dest);
    *     Mat2d.rotate(dest, dest, rad);
    *
    * @param out Mat2d receiving operation result
    * @param rad the angle to rotate the matrix by
    * @returns out
    */
    public static function fromRotation( ?out : Mat2d, rad : Float ) : Mat2d {
        if( out == null ) out = Mat2dTools.create();
        var s = Math.sin(rad), c = Math.cos(rad);
        out[0] = c;
        out[1] = s;
        out[2] = -s;
        out[3] = c;
        out[4] = 0;
        out[5] = 0;
        return out;
    }

    /**
    * Creates a matrix from a vector scaling
    * This is equivalent to (but much faster than):
    *
    *     Mat2d.identity(dest);
    *     Mat2d.scale(dest, dest, vec);
    *
    * @param out Mat2d receiving operation result
    * @param v Scaling vector
    * @returns out
    */
    public static function fromScaling( ?out : Mat2d, v : Vec2 ) : Mat2d {
        if( out == null ) out = Mat2dTools.create();
        out[0] = v[0];
        out[1] = 0;
        out[2] = 0;
        out[3] = v[1];
        out[4] = 0;
        out[5] = 0;
        return out;
    }

    /**
    * Creates a matrix from a vector translation
    * This is equivalent to (but much faster than):
    *
    *     Mat2d.identity(dest);
    *     Mat2d.translate(dest, dest, vec);
    *
    * @param out Mat2d receiving operation result
    * @param v Translation vector
    * @returns out
    */
    public static function fromTranslation( ?out : Mat2d, v : Vec2 ) : Mat2d {
        if( out == null ) out = Mat2dTools.create();
        out[0] = 1;
        out[1] = 0;
        out[2] = 0;
        out[3] = 1;
        out[4] = v[0];
        out[5] = v[1];
        return out;
    }
}
