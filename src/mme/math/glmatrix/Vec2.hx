package mme.math.glmatrix;

import mme.math.glmatrix.Vec2Tools;

#if lime
import lime.utils.Float32Array;
abstract Vec2(Float32Array) from Float32Array to Float32Array {
#else
import haxe.ds.Vector;
abstract Vec2(Vector<Float>) from Vector<Float> to Vector<Float> {
#end
    public inline function new() {
        #if lime
        this = new Float32Array(2);
        #else
        this = new Vector<Float>(2);
        this[0] = 0.0;
        this[1] = 0.0;
        #end
    }
    @:arrayAccess
    public inline function getk(key:Int) {
    return this[key];
    }
    @:arrayAccess
    public inline function setk(k:Int, v:Float):Float {
        this[k] = v;
        return v;
    }

    public var x ( get, set ) : Float;
    public var y ( get, set ) : Float;
    private inline function get_x() : Float {
        return this[0];
    }
    private inline function set_x( x : Float) : Float {
        return this[0] = x;
    }
    private inline function get_y() : Float {
        return this[1];
    }
    private inline function set_y( y : Float) : Float {
        return this[1] = y;
    }

    @:from
    static public function fromArray(a:Array<Float>) {
        return fromValues( a[0], a[1] );
    }
    @:to
    public function toArray() {
        return [this[0], this[1]];
    }
    public function toString() : String {
        return Vec2Tools.str(this);
    }

    /**
    * Creates a new Vec2 initialized with the given values
    *
    * @param x X component
    * @param y Y component
    * @returns a new 2D vector
    */
    public static function fromValues( ?out : Vec2, x : Float, y : Float ) : Vec2 {
        if( out == null ) out = Vec2Tools.create();
        out[0] = x;
        out[1] = y;
        return out;
    }
}
