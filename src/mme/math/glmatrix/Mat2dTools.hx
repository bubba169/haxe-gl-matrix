package mme.math.glmatrix;

import mme.math.glmatrix.GLMatrix;

import mme.math.glmatrix.Mat2d;
import mme.math.glmatrix.Vec2;

/**
 * 2x3 Matrix
 * @module Mat2d
 *
 * @description
 * A Mat2d contains six elements defined as:
 * <pre>
 * [a, c, tx,
 *  b, d, ty]
 * </pre>
 * This is a short form for the 3x3 matrix:
 * <pre>
 * [a, c, tx,
 *  b, d, ty,
 *  0, 0, 1]
 * </pre>
 * The last row is ignored so the array is shorter and operations are faster.
 */
class Mat2dTools {

/*

//
// GENERATORS in Mat2d class
//

Mat2d.fromValues( ?out : Mat2d, a : Float, b : Float, c : Float, d : Float, tx : Float, ty : Float ) : Mat2d;
Mat2d.fromRotation( ?out : Mat2d, rad : Float, ?out : Mat2d ) : Mat2d;
Mat2d.fromScaling( ?out : Mat2d, v : Vec2, ?out : Mat2d ) : Mat2d;
Mat2d.fromTranslation( ?out : Mat2d, v : Vec2, ?out : Mat2d ) : Mat2d;


//
// GENERATORS
//
create() : Mat2d;
mat2d.clone( a : Mat2d ) : Mat2d;
?mat2d.identity( ?out : Mat2d ) : Mat2d;


//
// EDIT
//
mat2d.set( out : Mat2d, a : Float, b : Float, c : Float, d : Float, tx : Float, ty : Float ) : Mat2d;


//
// OPERATORS
//

// unary
//
mat2d.copy( a : Mat2d, ?out : Mat2d ) : Mat2d;
mat2d.invert( a : Mat2d, ?out : Mat2d ) : Mat2d;


// unary, non-Mat2d result
//
mat2d.determinant( a : Mat2d ) : Float;
mat2d.str( a : Mat2d ) : String;
mat2d.frob( a : Mat2d ) : Float;


// binary
//
mat2d.add( a : Mat2d, b : Mat2d, ?out : Mat2d ) : Mat2d;
mat2d.subtract( a : Mat2d, b : Mat2d, ?out : Mat2d ) : Mat2d;
mat2d.multiply( a : Mat2d, b : Mat2d, ?out : Mat2d ) : Mat2d;


// binary, non-Mat2d arg
//
mat2d.rotate( a : Mat2d, rad : Float, ?out : Mat2d ) : Mat2d;
mat2d.scale( a : Mat2d, v : Vec2, ?out : Mat2d ) : Mat2d;
mat2d.translate( a : Mat2d, v : Vec2, ?out : Mat2d ) : Mat2d;
mat2d.multiplyScalar( a : Mat2d, b : Float, ?out : Mat2d ) : Mat2d;
mat2d.multiplyVec2( a : Mat2d, v : Vec2, ?out : Vec2 ) : Vec2;


// binary, non-Mat2d result
//
mat2d.exactEquals( a : Mat2d, b : Mat2d ) : Bool;
mat2d.equals( a : Mat2d, b : Mat2d ) : Bool;


// ternary
//
mat2d.multiplyScalarAndAdd( a : Mat2d, b : Mat2d, scale : Float, ?out : Mat2d ) : Mat2d;

*/

    /**
    * Creates a new identity Mat2d
    *
    * @returns {Mat2d} a new 2x3 matrix
    */
    public static inline function create() : Mat2d {
    var out = new Mat2d();
    return out;
    }

    /**
    * Creates a new Mat2d initialized with values from an existing matrix
    *
    * @param {Mat2d} a matrix to clone
    * @returns {Mat2d} a new 2x3 matrix
    */
    public static function clone( a : Mat2d ) : Mat2d {
    var out = new Mat2d();
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
    out[4] = a[4];
    out[5] = a[5];
    return out;
    }

    /**
    * Copy the values from one Mat2d to another
    *
    * @param {Mat2d} out the receiving matrix
    * @param {Mat2d} a the source matrix
    * @returns {Mat2d} out
    */
    public static function copy( a : Mat2d, ?out : Mat2d ) : Mat2d {
    if( out == null ) out = create();
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
    out[4] = a[4];
    out[5] = a[5];
    return out;
    }

    /**
    * Set a Mat2d to the identity matrix
    *
    * @param {Mat2d} out the receiving matrix
    * @returns {Mat2d} out
    */
    public static function identity( ?out : Mat2d ) : Mat2d {
    if( out == null ) out = create();
    out[0] = 1;
    out[1] = 0;
    out[2] = 0;
    out[3] = 1;
    out[4] = 0;
    out[5] = 0;
    return out;
    }

    /**
    * Set the components of a Mat2d to the given values
    *
    * @param {Mat2d} out the receiving matrix
    * @param {Float} a Component A (index 0)
    * @param {Float} b Component B (index 1)
    * @param {Float} c Component C (index 2)
    * @param {Float} d Component D (index 3)
    * @param {Float} tx Component TX (index 4)
    * @param {Float} ty Component TY (index 5)
    * @returns {Mat2d} out
    */
    public static function set( out : Mat2d, a : Float, b : Float, c : Float, d : Float, tx : Float, ty : Float ) : Mat2d {
    out[0] = a;
    out[1] = b;
    out[2] = c;
    out[3] = d;
    out[4] = tx;
    out[5] = ty;
    return out;
    }

    /**
    * Inverts a Mat2d
    *
    * @param {Mat2d} out the receiving matrix
    * @param {Mat2d} a the source matrix
    * @returns {Mat2d} out
    */
    public static function invert( a : Mat2d, ?out : Mat2d ) : Mat2d {
    if( out == null ) out = create();
    var aa = a[0], ab = a[1], ac = a[2], ad = a[3];
    var atx = a[4], aty = a[5];

    var det = aa * ad - ab * ac;
    if( det == 0.0 ){
        return null;
    }
    det = 1.0 / det;

    out[0] = ad * det;
    out[1] = -ab * det;
    out[2] = -ac * det;
    out[3] = aa * det;
    out[4] = (ac * aty - ad * atx) * det;
    out[5] = (ab * atx - aa * aty) * det;
    return out;
    }

    /**
    * Calculates the determinant of a Mat2d
    *
    * @param {Mat2d} a the source matrix
    * @returns {Float} determinant of a
    */
    public static function determinant( a : Mat2d ) : Float {
    return a[0] * a[3] - a[1] * a[2];
    }

    /**
    * Multiplies two Mat2d's
    *
    * @param {Mat2d} out the receiving matrix
    * @param {Mat2d} a the first operand
    * @param {Mat2d} b the second operand
    * @returns {Mat2d} out
    */
    public static function multiply( a : Mat2d, b : Mat2d, ?out : Mat2d ) : Mat2d {
    if( out == null ) out = create();
    var a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3], a4 = a[4], a5 = a[5];
    var b0 = b[0], b1 = b[1], b2 = b[2], b3 = b[3], b4 = b[4], b5 = b[5];
    out[0] = a0 * b0 + a2 * b1;
    out[1] = a1 * b0 + a3 * b1;
    out[2] = a0 * b2 + a2 * b3;
    out[3] = a1 * b2 + a3 * b3;
    out[4] = a0 * b4 + a2 * b5 + a4;
    out[5] = a1 * b4 + a3 * b5 + a5;
    return out;
    }

    /**
    * Rotates a Mat2d by the given angle
    *
    * @param {Mat2d} out the receiving matrix
    * @param {Mat2d} a the matrix to rotate
    * @param {Float} rad the angle to rotate the matrix by
    * @returns {Mat2d} out
    */
    public static function rotate( a : Mat2d, rad : Float, ?out : Mat2d ) : Mat2d {
    if( out == null ) out = create();
    var a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3], a4 = a[4], a5 = a[5];
    var s = Math.sin(rad);
    var c = Math.cos(rad);
    out[0] = a0 *  c + a2 * s;
    out[1] = a1 *  c + a3 * s;
    out[2] = a0 * -s + a2 * c;
    out[3] = a1 * -s + a3 * c;
    out[4] = a4;
    out[5] = a5;
    return out;
    }

    /**
    * Scales the Mat2d by the dimensions in the given Vec2
    *
    * @param {Mat2d} out the receiving matrix
    * @param {Mat2d} a the matrix to translate
    * @param {Vec2} v the Vec2 to scale the matrix by
    * @returns {Mat2d} out
    **/
    public static function scale( a : Mat2d, v : Vec2, ?out : Mat2d ) : Mat2d {
    if( out == null ) out = create();
    var a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3], a4 = a[4], a5 = a[5];
    var v0 = v[0], v1 = v[1];
    out[0] = a0 * v0;
    out[1] = a1 * v0;
    out[2] = a2 * v1;
    out[3] = a3 * v1;
    out[4] = a4;
    out[5] = a5;
    return out;
    }

    /**
    * Translates the Mat2d by the dimensions in the given Vec2
    *
    * @param {Mat2d} out the receiving matrix
    * @param {Mat2d} a the matrix to translate
    * @param {Vec2} v the Vec2 to translate the matrix by
    * @returns {Mat2d} out
    **/
    public static function translate( a : Mat2d, v : Vec2, ?out : Mat2d ) : Mat2d {
    if( out == null ) out = create();
    var a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3], a4 = a[4], a5 = a[5];
    var v0 = v[0], v1 = v[1];
    out[0] = a0;
    out[1] = a1;
    out[2] = a2;
    out[3] = a3;
    out[4] = a0 * v0 + a2 * v1 + a4;
    out[5] = a1 * v0 + a3 * v1 + a5;
    return out;
    }

    /**
    * Returns a string representation of a Mat2d
    *
    * @param {Mat2d} a matrix to represent as a string
    * @returns {String} string representation of the matrix
    */
    public static function str( a : Mat2d ) : String {
    return 'Mat2d(' + a[0] + ', ' + a[1] + ', ' + a[2] + ', ' +
            a[3] + ', ' + a[4] + ', ' + a[5] + ')';
    }

    /**
    * Returns Frobenius norm of a Mat2d
    *
    * @param {Mat2d} a the matrix to calculate Frobenius norm of
    * @returns {Float} Frobenius norm
    */
    public static function frob( a : Mat2d ) : Float {
    return(Math.sqrt(Math.pow(a[0], 2) + Math.pow(a[1], 2) + Math.pow(a[2], 2) + Math.pow(a[3], 2) + Math.pow(a[4], 2) + Math.pow(a[5], 2) + 1));
    }

    /**
    * Adds two Mat2d's
    *
    * @param {Mat2d} out the receiving matrix
    * @param {Mat2d} a the first operand
    * @param {Mat2d} b the second operand
    * @returns {Mat2d} out
    */
    public static function add( a : Mat2d, b : Mat2d, ?out : Mat2d ) : Mat2d {
    if( out == null ) out = create();
    out[0] = a[0] + b[0];
    out[1] = a[1] + b[1];
    out[2] = a[2] + b[2];
    out[3] = a[3] + b[3];
    out[4] = a[4] + b[4];
    out[5] = a[5] + b[5];
    return out;
    }

    /**
    * Subtracts matrix b from matrix a
    *
    * @param {Mat2d} out the receiving matrix
    * @param {Mat2d} a the first operand
    * @param {Mat2d} b the second operand
    * @returns {Mat2d} out
    */
    public static function subtract( a : Mat2d, b : Mat2d, ?out : Mat2d ) : Mat2d {
    if( out == null ) out = create();
    out[0] = a[0] - b[0];
    out[1] = a[1] - b[1];
    out[2] = a[2] - b[2];
    out[3] = a[3] - b[3];
    out[4] = a[4] - b[4];
    out[5] = a[5] - b[5];
    return out;
    }

    /**
    * Multiply Mat2d with Vec2
    * out = a * v
    *
    * @param {Vec2} out the receiving vector
    * @param {Mat2d} a matrix to be multiplied by vector
    * @param {Vec2} v the vector to multiply matrix with
    * @returns {Vec2} out
    */
    public static function multiplyVec2( a : Mat2d, v : Vec2, ?out : Vec2 ) : Vec2 {
    if( out == null ) out = Vec2Tools.create();
    var x = v[0],
        y = v[1];
    out[0] = a[0] * x + a[2] * y + a[4];
    out[1] = a[1] * x + a[3] * y + a[5];
    return out;
    }

    /**
    * Multiply each element of the matrix by a scalar.
    *
    * @param {Mat2d} out the receiving matrix
    * @param {Mat2d} a the matrix to scale
    * @param {Float} b amount to scale the matrix's elements by
    * @returns {Mat2d} out
    */
    public static function multiplyScalar( a : Mat2d, b : Float, ?out : Mat2d ) : Mat2d {
    if( out == null ) out = create();
    out[0] = a[0] * b;
    out[1] = a[1] * b;
    out[2] = a[2] * b;
    out[3] = a[3] * b;
    out[4] = a[4] * b;
    out[5] = a[5] * b;
    return out;
    }

    /**
    * Adds two Mat2d's after multiplying each element of the second operand by a scalar value.
    *
    * @param {Mat2d} out the receiving vector
    * @param {Mat2d} a the first operand
    * @param {Mat2d} b the second operand
    * @param {Float} scale the amount to scale b's elements by before adding
    * @returns {Mat2d} out
    */
    public static function multiplyScalarAndAdd( a : Mat2d, b : Mat2d, scale : Float, ?out : Mat2d ) : Mat2d {
    if( out == null ) out = create();
    out[0] = a[0] + (b[0] * scale);
    out[1] = a[1] + (b[1] * scale);
    out[2] = a[2] + (b[2] * scale);
    out[3] = a[3] + (b[3] * scale);
    out[4] = a[4] + (b[4] * scale);
    out[5] = a[5] + (b[5] * scale);
    return out;
    }

    /**
    * Returns whether or not the matrices have exactly the same elements in the same position (when compared with ==)
    *
    * @param {Mat2d} a The first matrix.
    * @param {Mat2d} b The second matrix.
    * @returns {Boolean} True if the matrices are equal, false otherwise.
    */
    public static function exactEquals( a : Mat2d, b : Mat2d ) : Bool {
    return a[0] == b[0] && a[1] == b[1] && a[2] == b[2] && a[3] == b[3] && a[4] == b[4] && a[5] == b[5];
    }

    /**
    * Returns whether or not the matrices have approximately the same elements in the same position.
    *
    * @param {Mat2d} a The first matrix.
    * @param {Mat2d} b The second matrix.
    * @returns {Boolean} True if the matrices are equal, false otherwise.
    */
    public static function equals( a : Mat2d, b : Mat2d ) : Bool {
    var a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3], a4 = a[4], a5 = a[5];
    var b0 = b[0], b1 = b[1], b2 = b[2], b3 = b[3], b4 = b[4], b5 = b[5];
    return (Math.abs(a0 - b0) <= GLMatrix.EPSILON*Math.max(1.0, Math.max(Math.abs(a0), Math.abs(b0))) &&
            Math.abs(a1 - b1) <= GLMatrix.EPSILON*Math.max(1.0, Math.max(Math.abs(a1), Math.abs(b1))) &&
            Math.abs(a2 - b2) <= GLMatrix.EPSILON*Math.max(1.0, Math.max(Math.abs(a2), Math.abs(b2))) &&
            Math.abs(a3 - b3) <= GLMatrix.EPSILON*Math.max(1.0, Math.max(Math.abs(a3), Math.abs(b3))) &&
            Math.abs(a4 - b4) <= GLMatrix.EPSILON*Math.max(1.0, Math.max(Math.abs(a4), Math.abs(b4))) &&
            Math.abs(a5 - b5) <= GLMatrix.EPSILON*Math.max(1.0, Math.max(Math.abs(a5), Math.abs(b5))));
    }

    /**
    * Alias for {@link Mat2d.multiply}
    * @function
    */
    public static var mul = multiply;

    /**
    * Alias for {@link Mat2d.subtract}
    * @function
    */
    public static var sub = subtract;
}
